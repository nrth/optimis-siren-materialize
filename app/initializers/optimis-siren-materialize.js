import setupServiceInjections from 'optimis-siren-materialize/initializers/setup-service-injections';

export function initialize(application) {
    setupServiceInjections(application);
}

export default {
    name: 'optimis-siren-materialize',
    after: ['optimis-siren'],
    initialize
};
