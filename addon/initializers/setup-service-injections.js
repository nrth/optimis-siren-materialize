export default function setup(application) {
    // materialize
	application.inject('component', 'materialize', 'service:materialize');

    // toast
	application.inject('component', 'toast', 'service:toast');
	application.inject('controller', 'toast', 'service:toast');
	application.inject('route', 'toast', 'service:toast');
}
