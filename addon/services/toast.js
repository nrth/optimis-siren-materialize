import Ember from 'ember';

const { Service, run, $ } = Ember;
const { toast } = Materialize;

function normalizeMessage(message) {
	return $(`<span>${message}</span>`);
}

export default Service.extend({
	success(message) {
		run(null, toast, normalizeMessage(message), 3000, 'green accent1');
	},
	
	info(message) {
		run(null, toast, normalizeMessage(message), 3000, 'blue accent1');
	},

	warning(message) {
		run(null, toast, normalizeMessage(message), 3000, 'yellow accent1 black-text');
	},
	
	error(message) {
		run(null, toast, normalizeMessage(message), 3000, 'red accent1');
	}
});
