import Ember from 'ember';

const { Service, run, $ } = Ember;
const { scheduleOnce } = run;

export default Service.extend({
    init() {
        let types = ['text', 'password', 'email', 'url', 'tel', 'number', 'search'];

        types.forEach((v, i) => { types[i] = `input[type=${v}]`; });
        types.push('textarea');
        types.forEach((v, i) => { types[i] = `${v}:focus`; });

        let focusSelector = types.join();

        this.set('_focusSelector', focusSelector);
    },

    _updateTextFields() {
        let focusSelector = this.get('_focusSelector');
        let $el = $(focusSelector);

        Materialize.updateTextFields();
        $el.siblings('label, i').addClass('active');
    },

    updateTextFields() {
        scheduleOnce('afterRender', this, this._updateTextFields);
    }
});
