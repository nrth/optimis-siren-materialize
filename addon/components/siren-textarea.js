import SirenTextarea from 'optimis-siren/components/siren-textarea';

export default SirenTextarea.extend({
    didRender() {
        this._super(...arguments);
        this.materialize.updateTextFields();
    }
});
