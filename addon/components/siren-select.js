import SirenSelect from 'optimis-siren/components/siren-select';

export default SirenSelect.extend({
    didRender() {
        this._super(...arguments);

        let $select = this._getElement();
        let initialized = this.get('_initialized');

        if (!initialized) {
            $select.material_select();
            this.set('_initialized', true);
        }

        this.setDisabled($select, true);
    },

    willDestroyElement() {
        this._super(...arguments);

        let $select = this._getElement();

        if ($select.length > 0 && !this.get('isDestroyed')) {
            $select.material_select('destroy');
            this.set('_initialized', false);
        }
    },

    setDisabled($select, isMaterialize) {
        if (!isMaterialize) {
            return;
        }

        let disabled = this.getAttr('disabled');
        let readonly = this.getAttr('readonly');

        if (readonly || disabled) {
            $select.prop('disabled', true);
        } else {
            $select.prop('disabled', false);
        }

        $select.material_select();

        if (readonly && !disabled) {
            $select.siblings('.select-dropdown').prop('disabled', false);
        }
    }
});
