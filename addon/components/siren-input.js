import SirenInput from 'optimis-siren/components/siren-input';

export default SirenInput.extend({
	didRender() {
		this._super(...arguments);
		this.materialize.updateTextFields();
	}
});
