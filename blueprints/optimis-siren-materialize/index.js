/* jshint node: true */
'use strict';

module.exports = {
    normalizeEntityName() { },

    afterInstall() {
        let addons = this.addAddonsToProject({
            packages: [
                'https://bitbucket.org/nrth/optimis-siren'
            ]
        });
        let bowerPackages = this.addBowerPackagesToProject([
            { name: 'materialize', target: '~0.97.8' },
            { name: 'tether', target: '~1.2.0' }
        ]);

        return Promise.all([addons, bowerPackages]);
    }
};
