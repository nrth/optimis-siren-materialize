/* eslint-env node */
'use strict';

const mergeTrees = require('broccoli-merge-trees');
const funnel = require('broccoli-funnel');
const path = require('path');

module.exports = {
    name: 'optimis-siren-materialize',

    isDevelopingAddon() {
        return true;
    },

    included(app) {
        this._super.included.apply(this, arguments);

        this.app = app;

        this.importBrowserDependencies(app);
    },

    importBrowserDependencies(app) {
        let vendor = this.treePaths.vendor;

        // Bootstrap
        app.import({
            development: `${vendor}/materialize-css/dist/js/materialize.js`,
            production: `${vendor}/materialize-css/dist/js/materialize.min.js`
        });
    },

    treeForVendor(vendorTree) {
        return this.treeForBrowserVendor(vendorTree);
    },

    treeForBrowserVendor(vendorTree) {
        let trees = [];
        let materializeDistJsPath = path.dirname(require.resolve('materialize-css'));
        let materializePath = path.resolve(materializeDistJsPath, '../../');

        if (vendorTree) {
            trees.push(vendorTree);
        }

        trees.push(
            funnel(materializePath, {
                destDir: 'materialize-css',
                include: ['dist/**/*']
            })
        );

        return mergeTrees(trees);
    },

    treeForPublic(publicTree) {
        let trees = [];
        let materializeDistJsPath = path.dirname(require.resolve('materialize-css'));
        let materializePath = path.resolve(materializeDistJsPath, '../');

        if (publicTree) {
            trees.push(publicTree);
        }

        trees.push(
            funnel(materializePath, {
                include: ['fonts/roboto/*']
            })
        );

        return mergeTrees(trees);
    }
};
